import gql from "graphql-tag";

export const animesPage = gql`
  query animesPage($page: Int, $total: Int) {
    Page(page: $page, perPage: $total) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      media(type: ANIME) {
        id
        title {
          romaji
        }
        coverImage {
          large
        }
      }
    }
  }
`;

export const animesPage2 = gql`
  query animesPage2($page: Int, $total: Int) {
    Page(page: $page, perPage: $total) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      media(averageScore_greater: 85, type: ANIME) {
        id
        title {
          romaji
        }
        coverImage {
          large
        }
        description(asHtml: false)
        trending
        averageScore
        tags {
          name
        }
      }
    }
  }
`;

export const animeId = gql`
  query animeId($id: Int) {
    Media(id: $id) {
      id
      title {
        romaji
      }
      coverImage {
        large
      }
      description(asHtml: false)
      trending
      averageScore
      tags {
        name
      }
    }
  }
`;
