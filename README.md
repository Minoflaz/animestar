# AnimeStar

> Anime rating site fullfilling my otaku and web developer needs.

Developed using :

- [Nuxt](https://fr.nuxtjs.org/) : A fullstack framework for Vue using ServerSideRendered pages and lightweight configuration.
- [**Apollo** For Nuxt](https://github.com/nuxt-community/apollo-module) : GraphQl implementation making server and client side GraphQl development easy (adapated for nuxt).
- [Vuetify](https://vuetifyjs.com/en/) : Provides tons of ready to use UI material components for Vue. 

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org) or [GraphQl Apollo docs](https://www.apollographql.com/).
